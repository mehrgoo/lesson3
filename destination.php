<?php
/**
 * Created by PhpStorm.
 * User: amirhossein
 * Date: 5/25/18
 * Time: 14:25
 */

error_reporting(10);
include_once "PublicMethods.php";

$order = $_REQUEST['order'];

if ($order == "register") {
    $name = $_REQUEST['name'];
    $family = $_REQUEST['family'];

    if (isUserExist($name))
        echo "error: duplicate user";
    else {
        $outPut = array("name" => $name, "family" => $family);
        $js = json_encode($outPut);
        file_put_contents("user", $js);
        echo "user has been registered";
        logEvent("new user registered ($name $family)");
    }
}

if ($order == "login") {
    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];
    $fileContent = file_get_contents("user");
    $array = json_decode($fileContent, true);
    if ($username == $array['username'] && $password == $array['password']){
        header("Refresh:1;url=index.php") ;
        echo "login successfull";
    }else{
        header("Refresh:3;url=login.html") ;
        echo "Username/password is wrong";
    }


    logEvent("user logined ($username) ");
}


function isUserExist($name)
{
    $jsonContent = file_get_contents("user");
    $array = json_decode($jsonContent, true);
    if ($array['name'] != $name)
        return false;
    else
        return true;
}


