<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html >
<head>
<meta http-equiv="content-type" content="text/html; charset="UTF-8"/>
<link rel="stylesheet" type="text/css" href="default.css" media="screen"/>

<title>mehrgoo</title>
</head>
<body>
<div class="container">
  <div class="gfx"><span></span></div>
  <div class="top">
    <div class="navigation" dir="rtl" lang="fa"> <a href="index.php" id="selected">صفحه اصلي</a> <a href="#">درباره ما</a> <a href="register_validate.php">عضويت</a> <a href="login.html">ورود</a> </div>
    <div class="pattern"><span></span></div>
    <div class="header">
      <h1>mehrgoo</h1>
      <p>Web Designer</p>
    </div>
    <div class="pattern"><span></span></div>
  </div>
  <div class="content">
    <div class="spacer"></div>
    <div class="item">
      <div class="title">Welcome to mysait</div>
      <div class="metadata"><b><h1> به سايت ما خوش آمديد</h1></b></div>
      <div class="body">
<?php
/**
 * Created by PhpStorm.
 * User: amirhossein
 * Date: 5/25/18
 * Time: 17:01
 */

if (!empty($_REQUEST['register'])) {
    $username  = $_REQUEST['username'];
    $password  = $_REQUEST['password'];
    $ip        = $_REQUEST['ip'];
    $email     = $_REQUEST['email'];
    $website   = $_REQUEST['website'];

    $userError = "" ;
    $passwordError = "" ;
    $ipError = "" ;
    $emailError = "" ;
    $webSiteError = "" ;

    if( empty($username)){
        $userError = " نام کاربری اجباری است" ;
    }
    if( empty($password)){
        $passwordError = " ورود پسورد اجباری است" ;
    }
    if( !filter_var($ip, FILTER_VALIDATE_IP)){
        $ipError = "فرمت ip اشتباه است" ;
    }
    if( !filter_var($email, FILTER_VALIDATE_EMAIL)){
        $emailError = "فرمت ایمیل اشتباه است" ;
    }
    if( !filter_var($website, FILTER_VALIDATE_URL)){
        $webSiteError = "فرمت وب سایت اشتباه است" ;
    }

    if( empty($userError) && empty($passwordError)&& empty($ipError)&& empty($emailError) && empty($webSiteError)){
        //register
        echo "<br>ثبت نام شما با موفقیت انجام شد <br> " ;
    }

}
$outPut = array("username" => $username, "password" => $password , "ip" => "$ip");
$js = json_encode($outPut);
file_put_contents("user", $js);
?>


<form action="?" method="POST">

    <table border="1">
        <tr>
            <td><input type="text" name="username" placeholder="Enter username"</td>
            <td><?=$userError?></td>
        </tr>
         <tr>
            <td><input type="password" name="password" placeholder="Enter password"</td>
            <td><?=$passwordError?></td>
        </tr>
        <tr>
            <td><input type="ip" name="ip" placeholder="Enter IP"</td>
            <td><?=$ip?></td>
        </tr>

        <tr>
            <td><input type="text" name="email" placeholder="Enter email"</td>
            <td><?=$emailError?></td>
        </tr>
        <tr>
            <td><input type="text" name="website" placeholder="Enter website"</td>
            <td><?=$webSiteError?></td>
        </tr>

        <tr>
            <td >
                <input type="submit" name="register"  value="Register">
            </td>
        </tr>

    </table>


</form>


      
      <div class="footer"> &copy; 2018 <a href="#">mehrgou.ir</a>. Valid <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> &amp; <a href="http://validator.w3.org/check?uri=referer">XHTML</a>. Template design by <a href="http://arcsin.se">akram ahmadian mehrgoo</a> </div>
</div>
</body>
</html>






