<?php
/**
 * Created by PhpStorm.
 * User: amirhossein
 * Date: 5/18/18
 * Time: 17:35
 */

function connectToDB()
{

}


function printSpace()
{
    echo "<br>--------------<br>";

}

function cleanWord($word)
{
    $outPut = trim($word);
    $outPut = stripcslashes($outPut);
    $outPut = htmlspecialchars($outPut);
    $outPut = str_replace("۰", "0", $outPut);
    $outPut = str_replace("۱", "1", $outPut);
    $outPut = str_replace("۲", "2", $outPut);
    $outPut = str_replace("۳", "3", $outPut);
    $outPut = str_replace("۴", "4", $outPut);
    $outPut = str_replace("۵", "5", $outPut);
    $outPut = str_replace("۶", "6", $outPut);
    $outPut = str_replace("۷", "7", $outPut);
    $outPut = str_replace("۸", "8", $outPut);
    $outPut = str_replace("۹", "9", $outPut);
    $outPut = str_replace("ي", "ی", $outPut);
    $outPut = str_replace("ك", "ک", $outPut);
    return $outPut;
}


function logEvent($msg)
{
    date_default_timezone_set('Asia/Tehran');
    $date = date('Y-m-d H:i:s', time());
    $userIP = $_SERVER['REMOTE_ADDR'];
    file_put_contents("log",
        $date . " " .
        $userIP . " " .
        $msg . "\n",
        FILE_APPEND);
}